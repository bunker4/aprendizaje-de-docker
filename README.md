Material correspondiente al [tutorial de Docker Compose brindado por Cristian Chiera](https://gitlab.com/bunker4/infraestructura-clasica/-/wikis/home/Tutorial-docker-compose) para el [grupo de aprendizaje Bunker4](https://gitlab.com/bunker4/infraestructura-clasica/-/wikis/home)

Links relacionados

Material extra de apoyo

- https://gitlab.com/pancutan/capacitacion-para-supercanal-en-docker/ - Sergio Alonso
- https://onedrive.live.com/?id=A72E8B952EF4E78A%2125888&cid=A72E8B952EF4E78A - Javier Tassi